# Babyfoot Manager

## Installation

1) Configurer les informations de connexion à la base postgres dans le fichier **src/db-config.json**.<br/>
2) Installer les dépendances avec la commande `npm install --production`<br/>
3) Créer la base de données avec la commande `npm run deploy`<br/>
**Attention**, il faut que la base par défaut *postgres* existe pour que le script fonctionne.<br/>
C'est installation a été testée sur Windows 7 mais devrait fonctionner sur n'importe quel OS.

## Utilisation
1) Lancer le serveur avec la commande `npm start`<br/>
2) L'app est disponible à l'addresse **localhost:8080**<br/>
