window.onload = function() {
	var $games = document.getElementById('games');
	var $count = document.getElementById('active-games-count');
	var socket = io.connect(window.location.href);


	// Socket io
	socket.emit('init', {}, function(games) {
		games.forEach(addGame);
	})

	socket.on('game-added', addGame);
	socket.on('game-updated', updateGame);
	socket.on('game-deleted', deleteGame);


	// Filters
	[].forEach.call(document.getElementsByClassName('filter'), function(filter) {
		filter.addEventListener('click', function() {
			if (filter.classList.contains('active')) {
				return;
			}

			document.querySelector('.filter.active').classList.remove('active');
			filter.classList.add('active');

			[].forEach.call(document.querySelectorAll('#games > .game'), function(game) {
				applyFilter(game, filter.id);
			})
		})
	})

	function applyFilter(div, filter) {
		filter = filter || document.querySelector('.filter.active').id;

		div.classList.remove('hide');
		var isFinishedGame = div.classList.contains('inactive');

		if (
			(filter === 'filter-active' && isFinishedGame) ||
			(filter === 'filter-inactive' && !isFinishedGame)
		) {
			div.classList.add('hide');
		}
	}

	// Active games count
	function updateActiveGamesCount() {
		$count.textContent = document.querySelectorAll('.game.active').length;
	}

	// Game form
	document.getElementById('add-game').addEventListener('click', function() {
		var input = document.getElementById('new-game-title');

		if (input.value === '') {
			return;
		}

		socket.emit('add-game', input.value, function(error) {
			if (error) {
				alert(error)
			} else {
				input.value = '';
			}
		})
	});


	// Game
	function onGameChange(event, div, game) {
		if (event === 'add' || event === 'update') {
			bindGameData(div, game);
			applyFilter(div);
		}

		updateActiveGamesCount();
	}

	function addGame(game) {
		var div = document.querySelector('#template > .game').cloneNode(true);
		div.id = 'game' + game.id;

		div.querySelector('.status').addEventListener('change', function(e) {
			socket.emit('update-game-status', { id: game.id, status: e.target.checked });
		})
		div.querySelector('.delete').addEventListener('click', function() {
			socket.emit('delete-game', game.id);
		})

		insertBefore($games, div);
		onGameChange('add', div, game);
	}

	function updateGame(game) {
		var div = document.getElementById('game' + game.id);

		// Game not find for some reasons, create it
		if (div === null) {
			return addGame(game);
		}

		onGameChange('update', div, game);
	}

	function deleteGame(id) {
		var div = document.getElementById('game' + id);
		if (div) {
			$games.removeChild(div);
			onGameChange('delete');
		}
	}

	function bindGameData(div, game) {
		div.querySelector('.status').checked = game.finished;
		div.querySelector('.title').textContent = game.title;

		div.classList.remove('active', 'inactive');
		div.classList.add(game.finished ? 'inactive' : 'active');
	}

	// Utils
	function insertBefore(parent, div) {
		if (parent.firstElementChild !== null) {
			parent.insertBefore(div, parent.firstElementChild);
		} else {
			parent.appendChild(div);
		}
	}
}
