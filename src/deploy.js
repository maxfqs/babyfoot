var { Client } = require('pg');
var DBConfig = require('./db-config');

async function createDatabase() {
    var db = new Client({
        user: DBConfig.user,
        password: DBConfig.password,
        host: DBConfig.host,
        database: 'postgres',
        port: DBConfig.port
    })

    try {
        await db.connect();
        await db.query(`DROP DATABASE IF EXISTS ${DBConfig.database}`);
        await db.query(`CREATE DATABASE ${DBConfig.database}`);

    } catch (error) {
        console.log(error);
        process.exit(1);
    }

    db.end();
    createTables();
}

async function createTables() {
    var db = new Client(DBConfig);

    try {
        await db.connect();
        await db.query('\
            CREATE TABLE games (\
                id SERIAL PRIMARY KEY,\
                title VARCHAR(150) not null,\
                finished BOOLEAN DEFAULT FALSE\
            )'
        );

    } catch (error) {
        console.log(error);
        process.exit(1);
    }

    db.end();
    console.log('Database created');
}

createDatabase();
