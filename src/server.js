var express = require('express');
var http = require('http');
var path = require('path');
var { Client } = require('pg');
var socketIO = require('socket.io');
var DBConfig = require('./db-config');

var app = express();
var server = http.createServer(app);
var io = socketIO(server);
var db = new Client(DBConfig);


app.use(express.static(__dirname));

app.get('/', function(req, res) {
	res.sendFile(path.join(__dirname, 'index.html'));
})
app.get('*', function(req, res) {
	res.redirect('/');
})


io.sockets.on("connection", function(socket) {

	socket.on('init', async function(args, cb) {
		var { rows: games } = await db.query('SELECT * FROM games ORDER BY id ASC');
		cb(games);
	})

	socket.on('add-game', async function(title, cb) {
		title = title.trim();

		if (title.length > 150) {
			return cb('Le nom de votre partie doit faire moins de 150 charactères');
		}

		var { rows: result } = await db.query('INSERT INTO games(title) values($1) RETURNING *', [title]);
		io.emit('game-added', result[0]);
		cb(false);
	})

	socket.on('update-game-status', async function(args) {
		var { rows: result } = await db.query('UPDATE games SET finished=($1) WHERE id=($2) RETURNING *', [args.status, args.id]);
		io.emit('game-updated', result[0]);
	})

	socket.on('delete-game', function(id) {
		db.query('DELETE FROM games WHERE id=($1)', [id]);
		io.emit('game-deleted', id);
	})
})


async function startServer() {
	await db.connect();
	server.listen(8080);

	console.log('server started on port 8080');
}

startServer();
